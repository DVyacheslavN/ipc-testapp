//#include "ConnectivityEngine.h"

//#include "testclient.h"

#include <thread>
#include <iostream>
#include <memory>
#include <unistd.h>
#include <cstdlib>

using namespace std;
#include <string>
#include <system_error>

#define SERVER_TEST
//#define STREAM_TEST

#ifdef SERVER_TEST

#include <ClientIface.h>
#include <ServerIface.h>

#include <filesystem>


enum caseId {
    LeftTop = 0,
    RightTop,
    LeftBottom,
    RightBottom
};

const int Dims[] = {4,26,687,26,3,400,687,400,675,344};
std::vector<int> getDim(int caseId)
{
    std::vector<int>array{};
    array.resize(4);
    switch (caseId) {

    case LeftTop: //"left-top"
       array = {Dims[0], Dims[1], Dims[8], Dims[9]};
        break;
    case RightTop: //"right-top"
       array = {Dims[2], Dims[3], Dims[8], Dims[9]};
        break;
    case LeftBottom: //"left-bottom"
       array = {Dims[4], Dims[5], Dims[8], Dims[9]};
        break;
    case RightBottom: //"right-bottom"
       array = {Dims[6], Dims[7], Dims[8], Dims[9]};
        break;
    }
    return array;
}
#endif

int main()
{
    cout << "Hello World!" << std::endl;

    std::cout << "What would you want to create:" << std::endl
              << "1: Server" << std::endl
              << "2: Client" << std::endl;
    int select;
    std::cin >> select;


    auto pid = std::to_string(static_cast<long>(getpid()));
    std::string terminal("");

    std::string logDir =std::filesystem::current_path();
    std::string comand(logDir + "/logTerminal.sh");
    comand.append(" ").append(pid).append(" ").append(logDir);

    auto size = getDim(select == 1 ? caseId::LeftBottom : caseId::RightBottom);
    std::string geometry = std::to_string(size.at(2));
    geometry.append("x").append(std::to_string(size.at(3))).append("+")
            .append(std::to_string(size.at(0))).append("+").append(std::to_string(size.at(1)));
    if( fork() == 0 ) {

       execlp("konsole", "konsole", "--geometry", geometry.data(), "-e",comand.data(), NULL );
       // std::system(comand.data());
    }

    //std::system(comand.data());


    std::cout << "Default IP: \"127.0.0.1\" param 0.\nEnter IP address:";
    std::string ipAddress;
    std::cin >> ipAddress;
    if (ipAddress == "0")
    {
        ipAddress = "127.0.0.1";
        std::cout << "Skip set ip, default ip: \"" << ipAddress << "\" was used\n";
    }

    std::cout << "Default Port: \"2323\" param 0.\nEnter port:";
    ushort port = 0;
    std::cin >> port;
    if (port == 0)
    {
        port = 2323;
        std::cout << "Skip set port, default port: \"" << std::to_string(port) << "\" was used\n";
    }

    int typeOfSending = 1;

    std::string sendMesg = "SendData";
    if (select == 1)
    {
        std::cerr << "Create Server";
        auto server = ServerIface::createServer(ipAddress, port);
        std::cout << sendMesg << " for first session:";
        std::string msg;
        do {
            if (server->sessionIds().size() != 0)
            {
                printf("select Session from\n");
                for (auto id : server->sessionIds())
                {
                    printf("Session: %d\n", id);
                }

                int sessionIndex = 0;
                std::cin >> sessionIndex;

                auto session = server->getSessionById(sessionIndex);
                std::cout << std::endl << "session.sessionStatus:" << session.sessionStatus()
                             ? "Connected\n" : "Disconected\n";

                msg = "";
                std::cin >> msg;
                if (typeOfSending == 1)
                {
                    if (session.isValid())
                    {
                        std::cout << "SEND PACKAGE";
                        Package pac(msg);
                        session.sendPackage(pac);
                    }
                }
            }
            sleep(2);
            std::cout << "waiting for Client" << std::endl;
        } while (msg != "q");
    }
    else
        if (select == 2)
        {
            std::cerr << "Create Client" << std::endl;
            auto clientInstance = ClientIface::createClient(ipAddress, port);

            std::cout << sendMesg << std::endl;
            std::string msg;

            do {
                msg = "";
                std::cin >> msg;

                Package pac(msg);

                if (typeOfSending == 1)
                {
                    clientInstance->sendPackage(pac);
                }


            } while (msg != "q");
        }

    std::cerr << "END SESSION";
    return 0;
}
