#ifndef DEBUG_H
#define DEBUG_H
#include <string>
#include <cstring>
#include <iostream>
#include <regex>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <ctime>
#include <mutex>

#include <unistd.h>
#include <chrono>

// ...
#include <sys/types.h>
#include <unistd.h>

#define LOG_TO_FILE
#define PID
//#define APP_NAME
//#define COMPILE_APP_NAME "compile"
//#define PROJECT_NAME ""



namespace  {

std::mutex MUTEX;
}

using namespace std::chrono;
using std::ofstream;

const milliseconds startSystem = duration_cast<milliseconds>(
            system_clock::now().time_since_epoch());

#ifndef COMPILE_APP_NAME
#define COMPILE_APP_NAME ""
#endif

const std::string app_name = COMPILE_APP_NAME;

class Debug
{
    std::string m_className = "";
    ofstream *m_fout;

#ifdef APP_NAME
public:
    static std::string m_Application_name;
private:
#endif

    std::string m_logFile = std::string("log")
            .append(app_name)
#ifdef APP_NAME
            .append(m_Application_name)
#endif
#ifdef PID
            .append(std::to_string((long)getpid()))
#endif
            .append(".log");

    template<typename T>
    void logoutFile(T logStr)
    {

        m_fout->open(m_logFile, ofstream::ios_base::out | ofstream::ios_base::app);
        //std::cerr << "LOG";
        if (!m_fout->is_open()) {
            setClasstext(__PRETTY_FUNCTION__);
            std::cerr <<":" << " log file isn't  opened" << std::endl ;
        }

        *m_fout << logStr << " "; // запись строки в файл
        m_fout->close();
    }

//    template<class T>
//    void ffff(T ddd)
//    {

//    }

//    template<int>
//    void ffff(int ddd)
//    {

//    }

public:

    Debug(const char *where = "default")
    {
        std::time_t result = std::time(nullptr);
        std::string time(std::asctime(std::localtime(&result)));
        milliseconds ms = duration_cast< milliseconds >(
            system_clock::now().time_since_epoch()) - startSystem;

        time.erase(--time.end());
        time.append(" ");
        setClasstext(where);
        m_fout = new ofstream ();
        m_fout->open(m_logFile, ofstream::ios_base::out | ofstream::ios_base::app);
        *m_fout << time << "ms:"<< std::to_string(ms.count()) << " " << m_className << " |";
        m_fout->close();
    }

    ~Debug()
    {
        m_fout->open(m_logFile, ofstream::ios_base::out | ofstream::ios_base::app);
        *m_fout << std::endl;
        m_fout->close();

        delete m_fout;
    }

    void setClasstext (const char *str)
    {
        if (str == nullptr) {
            return;
        }
        std::cmatch m;
        std::regex reg("\\S*\\(");
        std::regex_search (str, m, reg);
        m_className = m[0].str();
        m_className.erase( --m_className.end());
    }

    void write(const char *data, ulong size)
    {
        if (data == nullptr)
        {
            static char * mes = "\033[0;31m Data is null:\033[0m";
            data = mes;
        }

        m_fout->open(m_logFile, ofstream::ios_base::out | ofstream::ios_base::app);
        //std::cerr << "LOG";
        if (!m_fout->is_open()) {
            setClasstext(__PRETTY_FUNCTION__);
            std::cerr <<":" << " log file isn't  opened" << std::endl ;
        }

        m_fout->write(data, size); // запись строки в файл
        m_fout->close();
    }

    static void Pid()
    {
        std::cout << "Pid: " << std::to_string(static_cast<long>(getpid())) << std::endl;
    }

    template<class M>
    Debug &operator << (M logStr)
    {
#ifdef LOG_TO_FILE
        ::MUTEX.lock();
        logoutFile(logStr);
        ::MUTEX.unlock();
#else
        std::cerr <<" :" << str;
#endif
        return  *this;
    }
};

struct Cork{
    template<class T>
    Cork & operator <<(T) {return *this;}
    template<class T, class O>
    void write(T, O) {}
};


#define mDebug() Debug(__PRETTY_FUNCTION__) << "\033[1;33m Debug:\033[0m"
#define mMessage() Debug(__PRETTY_FUNCTION__) << "\033[1;32m Message:\033[0m"
#define mWarning() Debug(__PRETTY_FUNCTION__) << "\033[0;31m Warning:\033[0m"

#define mCork() Cork()
#define cr "\n"


#endif // DEBUG_H
