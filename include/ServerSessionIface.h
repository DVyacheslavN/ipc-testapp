#ifndef SESSIONIFACEPRIVATE_H
#define SESSIONIFACEPRIVATE_H

#include <Package.h>

#include <functional>
#include <memory>

class ServerSessionIface
{
public:
    enum Status
    {
        Disconnected = 0,
        Connected,
        NotRespond,
    } typedef Status;

    virtual std::string ip() = 0;
    virtual Status sessionStatus() = 0;
    virtual bool sendPackage(const Package &package) = 0;

    virtual void bindPackageRecived(std::function<void (const Package &)> callBack_getPackage) = 0;
    virtual void bindSessionStatusChanged(std::function<void (Status)> handlerSessionStatusChanged) = 0;
    virtual void bindErrorRecived(std::function<void (std::string)>) = 0;

    virtual ~ServerSessionIface() {}
};

#endif // SESSIONIFACEPRIVATE_H
