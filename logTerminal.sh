#!/bin/bash
echo "PID = $1"
Pid=$1
WindowType=$2
      #0    1    2     3   4    5     6     7     8     9
      #x    y    x     y   x    y     x     y     w     h
Dims=("4" "26" "687" "26" "3" "400" "687" "400" "675" "344")
#Dim=()

case $WindowType in
  "left-top")
  Dim=(${Dims[0]}, ${Dims[1]}, ${Dims[8]}, ${Dims[9]})
    #echo -n "left-top"
    ;;

  "right-top")
  Dim=(${Dims[2]}, ${Dims[3]}, ${Dims[8]}, ${Dims[9]})
    #echo -n "right-top"
    ;;

  "left-bottom")
  Dim=(${Dims[4]}, ${Dims[5]}, ${Dims[8]}, ${Dims[9]})
    #echo -n "left-bottom"
    ;;

  "right-bottom")
  Dim=(${Dims[6]}, ${Dims[7]}, ${Dims[8]}, ${Dims[9]})
   # echo -n "right-bottom"
    ;;
    *)
  Dim=(5 5 500 500)
    
esac

#
#echo "Current dimension:" ${Dim[*]}

#  unset x y w h
#  eval $(xwininfo -id $(xdotool getactivewindow) |
#    sed -n -e "s/^ \+Absolute upper-left X: \+\([0-9]\+\).*/x=\1/p" \
#           -e "s/^ \+Absolute upper-left Y: \+\([0-9]\+\).*/y=\1/p" \
#           -e "s/^ \+Width: \+\([0-9]\+\).*/w=\1/p" \
#           -e "s/^ \+Height: \+\([0-9]\+\).*/h=\1/p" )
#  echo -n "$x $y $w $h"


#xdotool windowsize $(xdotool getactivewindow) ${Dim[2]} ${Dim[3]}

#xdotool windowmove $(xdotool getactivewindow) ${Dim[0]} ${Dim[1]}

tail -F logConnectivity$Pid.log
